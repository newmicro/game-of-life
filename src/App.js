import React, { useState, useEffect, useRef } from 'react';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

const App = (props) => {
  const seed_card_body_ref = useRef(null);
  const seed_ref = useRef(null);
  const gol_container_ref = useRef(null);
  const gol_ref = useRef(null);

  const speed = 225;            // the speed at which new generations are created (in ms)
  const board_radius = 25;      // width and height of the GOL and seed grids

  const [cells_t_minus_1, setCellsTMinus1] = useState(null);  // stores cells at Time: t-1
  const [cells_t, setCellsT] = useState(null);                // stores cells at Time: t
  const [cells_seed, setCellsSeed] = useState(null);          // stores the initial cells seed
  const [generation, setGeneration] = useState(0);            // an incrementing count of the generation
  const [running, setRunning] = useState(false);              // whether the simulation is currently running
  const [simulation_ended, setSimulationEnded] = useState(false); // whether the simulation has ended (cells at time T == time T-1)
  const [board_ready, setBoardReady] = useState(false);       // whether the board is ready for running the simulation

  let compute_timeout = null;

  useEffect(() => {
    resize();
    window.addEventListener('resize', resize);
  }, []);

  useEffect(() => {
    if(board_ready){
      run();
    }
  }, [board_ready]);

  useEffect(() => {
    setRunning(true);
    compute_timeout = setTimeout(() => compute(), speed);
    return () => clearTimeout(compute_timeout);
  }, [generation]);

  function resize() {
    stop();
    initBoards();
  }

  function initBoards() {
    if(seed_ref.current && seed_card_body_ref.current) {
      initGridDimensions(seed_ref.current, seed_card_body_ref.current.offsetWidth);
    }

    if(gol_ref.current && gol_container_ref.current) {
      initGridDimensions(gol_ref.current, gol_container_ref.current.offsetWidth);
    }

    let init_cells_t_minus_1 = defaultCellsOff();
    let init_cells_seed = defaultCellsOff();

    // set an initial seed
    let start_x = parseInt(board_radius/2 - (board_radius/2)/2);
    for(let i = 0; i < board_radius / 2; i++) {
      let y = parseInt(board_radius/2);
      turnOn(init_cells_t_minus_1[start_x + i][y]);
      turnOn(init_cells_t_minus_1[start_x + i][y]);

      turnOn(init_cells_seed[start_x + i][y]);
      turnOn(init_cells_seed[start_x + i][y]);
    }

    setSimulationEnded(false);
    setGeneration(0);
    setCellsTMinus1(init_cells_t_minus_1);
    setCellsT(defaultCellsOff());
    setCellsSeed(init_cells_seed);
    setBoardReady(true);
  }

  function stop() {
    setRunning(false);
    clearTimeout(compute_timeout);
  }

  function restart() {
    loadSeed();
  }

  function run() {
    setRunning(true);
    compute();
  }

  function toggleRunning() {
    running ? stop() : run();
  }

  function initGridDimensions(grid_dom, width) {
    grid_dom.style.width = width + "px";
    grid_dom.style.height = width + "px";
  }

  function defaultCellsOff() {
    let cells = new Array(board_radius);

    for(let x = 0; x < board_radius; x++) {
      cells[x] = new Array(board_radius);

      for(let y = 0; y < board_radius; y++) {
        cells[x][y] = { pos_x: x, pos_y: y, state: 'off'};
      }
    }
    return cells;
  }

  // if t-1 state and t state are identical, the simulation has ended
  function simulationHasEnded() {
    for(let x = 0; x < board_radius; x++) {
      for(let y = 0; y < board_radius; y++) {
        if(cells_t_minus_1[x][y].state != cells_t[x][y].state) {
          return false;
        }
      }
    }
    return true;
  }

  function turnOn(cell) {
    cell.state = 'on';
  }

  function turnOff(cell) {
    cell.state = 'off';
  }

  function getNeighbors(cell){
    let neighbors = [];

    if(cell.pos_x - 1 >= 0) {
      if(cell.pos_y - 1 >= 0) {
        neighbors.push(cells_t_minus_1[cell.pos_x - 1][cell.pos_y - 1]);
      }

      neighbors.push(cells_t_minus_1[cell.pos_x - 1][cell.pos_y]);

      if(cell.pos_y + 1 < board_radius) {
        neighbors.push(cells_t_minus_1[cell.pos_x - 1][cell.pos_y + 1]);
      }
    }

    if(cell.pos_x + 1 < board_radius) {
      if(cell.pos_y - 1 >= 0) {
        neighbors.push(cells_t_minus_1[cell.pos_x + 1][cell.pos_y - 1]);
      }

      neighbors.push(cells_t_minus_1[cell.pos_x + 1][cell.pos_y]);

      if(cell.pos_y + 1 < board_radius) {
        neighbors.push(cells_t_minus_1[cell.pos_x + 1][cell.pos_y + 1]);
      }
    }
    
    if(cell.pos_y - 1 >= 0) {
      neighbors.push(cells_t_minus_1[cell.pos_x][cell.pos_y - 1]);
    }

    if(cell.pos_y + 1 < board_radius) {
      neighbors.push(cells_t_minus_1[cell.pos_x][cell.pos_y + 1]);
    }
    return neighbors;
  }

  function compute() {
    // make a deep copy
    let new_cells_t = cells_t.map(item => ([...item]));

    // calculate the next generation
    for(let x = 0; x < board_radius; x++) {
      for(let y = 0; y < board_radius; y++) {
        let neighbors = getNeighbors(cells_t_minus_1[x][y]);
        let nbr_alive_neighbors = neighbors.filter(n => n.state == 'on').length;

        if(cells_t_minus_1[x][y].state == "on") {
          if(nbr_alive_neighbors === 2 || nbr_alive_neighbors === 3) {
            turnOn(new_cells_t[x][y]);
          }
          else {
            turnOff(new_cells_t[x][y]);
          }
        }
        else if(nbr_alive_neighbors == 3) {
          turnOn(new_cells_t[x][y]);
        }
      }
    }

    setCellsT(new_cells_t);

    if(simulationHasEnded()) {
      stop();
      setSimulationEnded(true);
      return;
    }

    // make a deep copy
    let new_cells_t_minus_1 = cells_t_minus_1.map(item => ([...item]));

    // turn on or off the old cells based on the new cells
    for(let x = 0; x < board_radius; x++) {
      for(let y = 0; y < board_radius; y++) {
        if(new_cells_t[x][y].state == 'on') {
          turnOn(new_cells_t_minus_1[x][y]);
        }
        else {
          turnOff(new_cells_t_minus_1[x][y]);
        }
      }
    }

    setGeneration(generation + 1);
    setCellsTMinus1(new_cells_t_minus_1);
  }

  function toggleSeedCell(x, y) {
    if(running) {
      stop();
    }

    let new_cells_seed = cells_seed.map(item => ([...item]));

    if(new_cells_seed[x][y].state == 'off') {
      turnOn(new_cells_seed[x][y]);
    }
    else {
      turnOff(new_cells_seed[x][y]);
    }

    setCellsSeed(new_cells_seed);
  }

  function loadSeed() {
    if(running) {
      stop();
    }

    // make a copy of the seed cells
    let cells = new Array(board_radius);
    for(let x = 0; x < board_radius; x++) {
      cells[x] = new Array(board_radius);

      for(let y = 0; y < board_radius; y++) {
        cells[x][y] = { pos_x: x, pos_y: y, state: cells_seed[x][y].state};
      }
    }

    setSimulationEnded(false);
    setCellsTMinus1(cells);
    setCellsT(defaultCellsOff());
    setGeneration(0);
  }

  function clearSeed() {
    if(running) {
      stop();
    }

    // make deep copy
    let new_cells_seed = cells_seed.map(item => ([...item]));
    for(let x = 0; x < board_radius; x++) {
      for(let y = 0; y < board_radius; y++) {
        turnOff(new_cells_seed[x][y]);
      }
    }

    setCellsSeed(new_cells_seed);
  }

  let seed_cell_width = seed_card_body_ref.current ? seed_card_body_ref.current.offsetWidth / board_radius : 0;
  let gol_cell_width = gol_ref.current ? gol_ref.current.offsetWidth / board_radius : 0;

  return (
    <div className="container-fluid">
      <div className="row mt-5 mb-2">
        <div className="col header text-center">
          <h1>Game Of Life Simulator</h1>

          <div className="mb-3">Generation: {generation} | Speed: {speed}ms | Radius: {board_radius} cells</div>

          <div className="mb-3">
            {!simulation_ended &&
              <button className="btn btn-danger" onClick={toggleRunning}>
                { running ? "Stop" : "Start" } 
              </button>
            }

            <button className="ml-2 btn btn-success" onClick={restart}>Restart</button>

            {simulation_ended && 
              <span className="text-light ml-2">Simulation Ended</span>
            }
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-12 col-lg-3 mb-5 mb-lg-0">
          <div className="card bg-light">
            <div className="card-header">Initial Seed</div>
            <div className="card-body">
              <div id="seed_card_body" className="mb-2" ref={seed_card_body_ref}>
                Click the cells below to edit the seed, and then click Load 
              </div>
              <div id="seed" ref={seed_ref}>
                {cells_seed && board_ready ? (
                  cells_seed.map((cell_x, x_index) => {
                    return cell_x.map((cell_y, y_index) => {
                      return <div key={'cell_seed_' + x_index + y_index} onClick={() => toggleSeedCell(x_index, y_index)} className={cell_y.state == 'off' ? 'cell off' : 'cell on'} style={{width: seed_cell_width, height: seed_cell_width, left: x_index*seed_cell_width, top: y_index*seed_cell_width}}></div>
                    })
                  })
                ) : null}
              </div>

              <button className="btn btn-light mt-2 float-left" onClick={clearSeed}>Clear</button>
              <button className="btn btn-primary mt-2 float-right" onClick={loadSeed}>Run</button>
            </div>
          </div>
        </div>

        <div className="col-12 col-lg-4 mx-auto text-center mb-5 mb-lg-0" id="gol-container" ref={gol_container_ref}>
          <div id="gol" ref={gol_ref}>
            {cells_t_minus_1 && board_ready ? (
              cells_t_minus_1.map((cell_x, x_index) => {
                return cell_x.map((cell_y, y_index) => {
                  return <div key={'cell_gol_' + x_index + y_index} className={cell_y.state == 'off' ? 'cell off' : 'cell on'} style={{width: gol_cell_width, height: gol_cell_width, left: x_index*gol_cell_width, top: y_index*gol_cell_width}}></div>
                })
              })
            ) : null}
          </div>
        </div>

        <div className="col-12 col-lg-3">
          <div className="card bg-light mb-2">
            <div className="card-header">Rules</div>
            <div className="card-body">
              <p className="card-text">A cell can be either alive (colored) or dead (white)</p>
              <p className="card-text">During each generation the state of the cell is computed from the following rules:</p>
              <ol>
                <li>If the cell is alive and has 2 or 3 alive neighboring cells, it stays alive</li>
                <li>If the cell is alive and has more than 3 alive neighboring cells, it dies</li>
                <li>If the cell is alive and has less than 2 alive neighboring cells, it dies</li>
                <li>If the cell is dead and has 3 alive neighboring cells, it becomes alive</li>
              </ol>

              <p className="card-text">
                Wikipedia: <a href="https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life" target="_blank">Game of Life</a>
              </p>

              <p className="card-text">
                Source Code:<br /> <a href="https://bitbucket.org/newmicro/game-of-life/src/master/" target="_blank">https://bitbucket.org/newmicro/game-of-life</a>
              </p>

              <p className="card-text">
                Programming credit: Blake Miller<br />
                <a href="https://blakescreations.com" target="_blank">blakescreations.com</a> | <a href="https://x.com/blakeleemiller" target="_blank">X</a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
# A ReactJS-based implementation of the Game Of Life

I built this out of curiousity - it's a work in progress but is fully functional.  It runs in a browser and allows you to create a seed pattern and see how the simulation plays out.  Most parameters, like the grid size and generation speed, are configurable via the App.js constructor.  

## Demo
[https://blakescreations.com/gol/index.html](https://blakescreations.com/gol/index.html)

## Set Up

In the root folder, install packages with:

`npm i`

To run:

`yarn client`

To build for the web (uses Webpack):

`yarn build`

## Game Of Life Explanation
[https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)

## TODOS - Future Development
1. Allow configuration of the board radius, speed, and radius via UI components
2. Add tests
3. Allow loading common/interesting GOL patterns/seeds via dropdown

Author: Blake Miller - <blakeleemiller@gmail.com> - 2020
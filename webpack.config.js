const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
var webpack = require("webpack");

module.exports = () => {
  const outputDirectory = 'dist';

  return {
    entry: './src/index.js',
    output: {
      path: path.join(__dirname, outputDirectory),
      filename: 'game_of_life.min.js',
      library: 'GameOfLife',
      libraryTarget: 'umd',
      clean: true,
      umdNamedDefine: true
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader'
          }
        },
        {
          test: /\.css$/,
          use: [
            {
              loader: 'style-loader', // creates style nodes from JS strings
            },
            {
              loader: 'css-loader', // translates CSS into CommonJS
            },
          ],
        },
        {
          test: /\.(png|jpg|woff|woff2|eot|ttf|svg|otf)$/,
          type: 'asset/resource'
        }
      ]
    },
    devServer: {
      port: 3000,
      open: '/',
    },
    plugins: [
      new webpack.ProvidePlugin({
        Buffer: ['buffer', 'Buffer'],
      }),
      new webpack.ProvidePlugin({
        process: 'process/browser',
      }),
      new HtmlWebpackPlugin({
        inject: 'body',
        favicon: './public/favicon.ico',
        template: './public/index.html',
        title: 'Game Of Life'
      }),
      new webpack.optimize.ModuleConcatenationPlugin(),
      new CompressionPlugin(),
    ]
  }
};